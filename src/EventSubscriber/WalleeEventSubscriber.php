<?php

namespace Drupal\wallee\EventSubscriber;

use Drupal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\wallee\WalleeService;

/**
 *
 */
class WalleeEventSubscriber implements EventSubscriberInterface {

  private $config_factory;

  private $wallee_srv;

  /**
   *
   */
  public function __construct(ConfigFactoryInterface $config_factory, WalleeService $wallee_srv) {
    $this->config_factory = $config_factory;
    $this->wallee_srv = $wallee_srv;
  }

  /**
   *
   */
  public static function getSubscribedEvents() {
    // $config = \Drupal::config('config.factory');
    // $createInvoiceOnTransition = $config->get("wallee.settings")->get("wallee_invoice_creation");
    // The format for adding a state machine event to subscribe to is:
    // {group}.{transition key}.pre_transition or {group}.{transition key}.post_transition
    // depending on when you want to react.
    // if(isset($createInvoiceOnTransition))
    // $events = [$createInvoiceOnTransition => 'onTriggerInvoiceCreation'];.
    $events = [
      'commerce_order.place.pre_transition' => 'onTriggerInvoiceCreation',
      // 'commerce_order.paid.post_transition' => 'onTriggerInvoiceCreation',.
      'commerce_order.deleted.post_transition' => 'onOrderDelete',
    ];

    return $events;
  }

  /**
   *
   */
  public function onOrderDelete(WorkflowTransitionEvent $event) {
    $this->wallee_srv->derecognizeInvoice($event->getEntity());
  }

  /**
   *
   */
  public function onTriggerInvoiceCreation(WorkflowTransitionEvent $event) {
    // value;.
    $order_id = $event->getEntity()->get("order_id")->getValue()[0]["value"];

    Drupal::logger('wallee')
      ->notice("invoice creation triggered for order:" . $order_id);

    $invoice_creation_states = explode(",", $this->config_factory->get("wallee.settings")
      ->get("wallee_invoice_creation"));
    $automatic_invoice_payment_methods = explode(",", $this->config_factory->get("wallee.settings")
      ->get("wallee_invoice_creation_per_payment_method"));

    if (count($automatic_invoice_payment_methods) > 0) {
      $order_payment_gateway = $this->wallee_srv->getPaymentGatewaysByOrderId($order_id);

      // If we dont find a payment method for this order, we need to stop.
      if (count($order_payment_gateway) != 1) {
        Drupal::logger('wallee')
          ->notice("could not find the payment method for order with id:" . $order_id . ". Therefore setting invoice as default.");
        return;
      }

      // If the selected payment gateway for this order is not in the ones that
      // need automatic invoice creation, bail out.
      if (!in_array($order_payment_gateway, $automatic_invoice_payment_methods)) {
        Drupal::logger('wallee')
          ->notice("the selected payment method does not support automatic invoice creation");
        return;
      }

    }

    $current_state = $event->getToState()->getId();

    if (in_array($current_state, $invoice_creation_states)) {

      $this->wallee_srv->createTransactionFromOrderId($order_id, WalleeService::PROCESS_QUEUE);
    }
    else {
      Drupal::logger('wallee')
        ->notice("invoice creation failed to execute because current state:" . $current_state . " is not in the group of states:" . implode(",", $invoice_creation_states));
    }
  }

}
