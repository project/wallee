<?php

namespace Drupal\wallee;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\wallee\Entity\InvoiceInterface;

/**
 * Access controller for the Invoice entity.
 *
 * @see \Drupal\wallee\Entity\Invoice.
 */
class InvoiceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var InvoiceInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished invoice entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published invoice entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit invoice entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete invoice entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add invoice entities');
  }

}
