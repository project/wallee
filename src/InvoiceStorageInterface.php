<?php

namespace Drupal\wallee;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\wallee\Entity\InvoiceInterface;

/**
 * Defines the storage handler class for Invoice entities.
 *
 * This extends the base storage class, adding required special handling for
 * Invoice entities.
 *
 * @ingroup wallee
 */
interface InvoiceStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Invoice revision IDs for a specific Invoice.
   *
   * @param InvoiceInterface $entity
   *   The Invoice entity.
   *
   * @return int[]
   *   Invoice revision IDs (in ascending order).
   */
  public function revisionIds(InvoiceInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Invoice author.
   *
   * @param AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Invoice revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param InvoiceInterface $entity
   *   The Invoice entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(InvoiceInterface $entity);

  /**
   * Unsets the language for all Invoice with the given language.
   *
   * @param LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
