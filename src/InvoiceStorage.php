<?php

namespace Drupal\wallee;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\wallee\Entity\InvoiceInterface;

/**
 * Defines the storage handler class for Invoice entities.
 *
 * This extends the base storage class, adding required special handling for
 * Invoice entities.
 *
 * @ingroup wallee
 */
class InvoiceStorage extends SqlContentEntityStorage implements InvoiceStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(InvoiceInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {invoice_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {invoice_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(InvoiceInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {invoice_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('invoice_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
