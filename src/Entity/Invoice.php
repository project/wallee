<?php

namespace Drupal\wallee\Entity;

use Drupal;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

// Use Drupal\Core\Field\FieldStorageDefinitionInterface;.

/**
 * Defines the Invoice entity.
 *
 * @ingroup wallee
 *
 * @ContentEntityType(
 *   id = "invoice",
 *   label = @Translation("Invoice"),
 *   handlers = {
 *     "storage" = "Drupal\wallee\InvoiceStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\wallee\InvoiceListBuilder",
 *     "views_data" = "Drupal\wallee\Entity\InvoiceViewsData",
 *     "translation" = "Drupal\wallee\InvoiceTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\wallee\Form\InvoiceForm",
 *       "add" = "Drupal\wallee\Form\InvoiceForm",
 *       "edit" = "Drupal\wallee\Form\InvoiceForm",
 *       "delete" = "Drupal\wallee\Form\InvoiceDeleteForm",
 *     },
 *     "access" = "Drupal\wallee\InvoiceAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\wallee\InvoiceHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "invoice",
 *   data_table = "invoice_field_data",
 *   revision_table = "invoice_revision",
 *   revision_data_table = "invoice_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer invoice entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "invoice_id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/invoice/{invoice}",
 *     "add-form" = "/admin/structure/invoice/add",
 *     "edit-form" = "/admin/structure/invoice/{invoice}/edit",
 *     "delete-form" = "/admin/structure/invoice/{invoice}/delete",
 *     "version-history" = "/admin/structure/invoice/{invoice}/revisions",
 *     "revision" =
 *   "/admin/structure/invoice/{invoice}/revisions/{invoice_revision}/view",
 *     "revision_revert" =
 *   "/admin/structure/invoice/{invoice}/revisions/{invoice_revision}/revert",
 *     "revision_delete" =
 *   "/admin/structure/invoice/{invoice}/revisions/{invoice_revision}/delete",
 *     "translation_revert" =
 *   "/admin/structure/invoice/{invoice}/revisions/{invoice_revision}/revert/{langcode}",
 *     "collection" = "/admin/structure/invoice",
 *   },
 *   field_ui_base_route = "invoice.settings"
 * )
 */
class Invoice extends RevisionableContentEntityBase implements InvoiceInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the invoice owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getInvoiceId() {
    return $this->get('invoice_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInvoiceId($invoice_id) {
    $this->set('invoice_id', $invoice_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderId() {
    return $this->get('order_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrderId($order_id) {
    $this->set('order_id', $order_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get('state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setState($state) {
    $this->set('state', $state);
    return $this;
  }

  /**
   *
   */
  public function getTransactionId($transaction_id) {
    return $this->get('$transaction_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTransactionId($transaction_id) {
    $this->set('transaction_id', $transaction_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   *
   */
  public function getInvoiceFile() {
    return $this->get("invoice_file")->value;
  }

  /**
   *
   */
  public function setInvoiceFile($invoice_file) {
    return $this->set("invoice_file", $invoice_file);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Invoice entity.'))
      ->setReadOnly(TRUE);

    // Invoice Number.
    $fields['invoice_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Invoice ID'))
      ->setDescription(t('The ID of the Invoice.'))
      ->setReadOnly(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ]);

    // Drupal Order Number.
    $fields['order_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Order ID'))
      ->setDescription(t('The ID of the Order.'))
      ->setReadOnly(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ]);

    // Transaction ID.
    $fields['transaction_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Transaction ID'))
      ->setDescription(t('The ID of the Transaction.'))
      ->setReadOnly(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ]);

    $fields["invoice_file"] = BaseFieldDefinition::create('file')
      ->setLabel('Invoice File')
      ->setSettings([
        'uri_scheme' => 'private',
        'file_directory' => 'invoices',
        'file_extensions' => 'pdf',
      ])
      // ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'file',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'file',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Invoice entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('State'))
      ->setDescription(t('The State of the Invoice entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Invoice is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
