<?php

namespace Drupal\wallee;

// Use Drupal\Core\Config\ConfigFactoryInterface;.
use Wallee\Sdk\Model\TransactionInvoice;
use Wallee\Sdk\ApiClient;
use Wallee\Sdk\Service\TransactionInvoiceService;
use Wallee\Sdk\Service\TransactionService;
use Wallee\Sdk\Service\ChargeFlowService;
use Wallee\Sdk\Service\ChargeAttemptService;
use Wallee\Sdk\Model\EntityQuery;
use Wallee\Sdk\ApiException;

/**
 *
 */
class WalleeWrapper {

  protected $spaceId;

  protected $appUserId;

  protected $appUserPwd;

  protected $transactionService;

  protected $transactionInvoiceService;

  protected $messenger;

  protected $chargeAttempt;

  /**
   *
   */
  public function __construct($wallee_config, $messenger) {
    $this->spaceId = $wallee_config->get('wallee_spaceId');
    $this->appUserId = $wallee_config->get('wallee_appUserId');
    $this->appUserPwd = $wallee_config->get('wallee_appUserPwd');

    $this->apiClient = new ApiClient($this->appUserId, $this->appUserPwd);

    $this->transactionInvoiceService = new TransactionInvoiceService($this->apiClient);
    $this->transactionService = new TransactionService($this->apiClient);
    $this->chargeAttempt = new ChargeAttemptService($this->apiClient);

    $this->messenger = $messenger;
  }

  /**
   *
   */
  public function derecognizeInvoice(TransactionInvoice $invoice) {
    $invoice_id = $invoice->getId();
    $this->transactionInvoiceService->markAsDerecognized($this->spaceId, $invoice_id);
  }

  /**
   *
   */
  public function searchTransactionByInvoice($invoice_id) {
    $data = [];
    $data["filter"] = [
      "fieldName" => "id",
      "operator" => "EQUALS",
      "type" => "LEAF",
      "value" => $invoice_id,
    ];
    $query = new EntityQuery($data);

    try {
      $res = $this->transactionInvoiceService->search($this->spaceId, $query);
      if (count($res) > 0) {
        foreach ($res as $inv) {
          return $inv->getLinkedTransaction();
        }
      }
    } catch (ApiException $e) {
      $this->walleeException($e);
    }

  }

  /**
   *
   */
  public function searchTransactionByOrderId($order_id) {
    $data = [];
    $data["filter"] = [
      "fieldName" => "merchantReference",
      "operator" => "EQUALS",
      "type" => "LEAF",
      "value" => $order_id,
    ];
    $query = new EntityQuery($data);

    try {
      $res = $this->transactionInvoiceService->search($this->spaceId, $query);
      if (count($res) > 0) {
        foreach ($res as $inv) {
          return $inv->getLinkedTransaction();
        }
      }
    } catch (ApiException $e) {
      $this->walleeException($e);
    }

  }

  /**
   *
   */
  public function buildPaymentPageUrl($transaction_id) {
    return $this->transactionService->buildPaymentPageUrl($this->spaceId, $transaction_id);
  }

  /**
   *
   */
  public function searchInvoiceByTransaction($transaction_id) {
    $data = [];
    $data["filter"] = [
      "fieldName" => "completion.lineItemVersion.transaction.id",
      "operator" => "EQUALS",
      "type" => "LEAF",
      "value" => $transaction_id,
    ];
    $query = new EntityQuery($data);
    $res = "";
    try {
      $res = $this->transactionInvoiceService->search($this->spaceId, $query);
    } catch (ApiException $e) {
      $this->walleeException($e);
    }

    return $res;
  }

  /**
   *
   */
  public function searchInvoiceByOrder($order_id) {
    $data = [];
    $data["filter"] = [
      "fieldName" => "completion.lineItemVersion.transaction.merchantReference",
      "operator" => "EQUALS",
      "type" => "LEAF",
      "value" => $order_id,
    ];
    $query = new EntityQuery($data);
    $res = "";
    try {
      $res = $this->transactionInvoiceService->search($this->spaceId, $query);
    } catch (ApiException $e) {
      $this->walleeException($e);
    }

    return $res;
  }

  /**
   *
   */
  public function getInvoiceById($invoice_id) {
    try {
      $invoice = $this->transactionInvoiceService->read($this->spaceId, $invoice_id);
      return $invoice;
    } catch (ApiException $e) {
      $this->walleeException($e);
    }
  }

  /**
   *
   */
  public function getInvoicesByTransactionId($transaction_id) {
    try {
      $invoice = $this->transactionService->getInvoiceDocument($this->spaceId, $transaction_id);
      return $invoice;
    } catch (ApiException $e) {
      $this->walleeException($e);
    }
  }

  /**
   *
   */
  public function createTransaction($transaction) {
    $transaction_id = 0;
    try {
      $result = $this->transactionService->create($this->spaceId, $transaction);
      $transaction_id = $result->getId();
      if (is_numeric($transaction_id) && $transaction_id > 0) {
        $this->finalizeTransactionCreation($transaction_id);
      }
    } catch (ApiException $e) {
      $this->walleeException($e);
    }

    return $transaction_id;
  }

  /**
   *
   */
  public function finalizeTransactionCreation($transaction_id) {
    try {
      $chargeFlowService = new ChargeFlowService($this->apiClient);
      $chargeFlowService->applyFlow($this->spaceId, $transaction_id);
    } catch (ApiException $e) {
      $this->walleeException($e);
    }
  }

  /**
   *
   */
  public function deleteInvoice($transaction_id) {

  }

  /**
   *
   */
  public function walleeException(ApiException $e) {
    $this->messenger->addMessage(t('An exception occurred in Wallee. Please try again.<br>Exception:' . $e->getResponseBody()->message));
  }

  /**
   *
   */
  public function getPaymentInfo($payment_id) {
    $data = [];
    $data["filter"] = [
      "fieldName" => "id",
      "operator" => "EQUALS",
      "type" => "LEAF",
      "value" => $payment_id,
    ];
    $query = new EntityQuery($data);
    $res = "";
    try {
      $res = $this->chargeAttempt->search($this->spaceId, $query);
    } catch (ApiException $e) {
      $this->walleeException($e);
    }

    return $res;
  }

}
