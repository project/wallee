<?php

namespace Drupal\wallee\Event;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Wraps a node insertion demo event for event listeners.
 */
class GetUserLanguageEvent extends Event {

  protected $customer;

  protected $language;

  const USER_PREFERRED_LANG = 'event_subscriber_wallee.user.lang';

  /**
   * Constructs a node insertion demo event object.
   *
   * @param EntityInterface $entity
   */
  public function __construct($user) {
    $this->customer = $user;
  }

  /**
   * Get the inserted entity.
   *
   * @return EntityInterface
   */
  public function getCustomer() {
    return $this->customer;
  }

  /**
   *
   */
  public function getLanguage() {
    return $this->language;
  }

  /**
   *
   */
  public function setLanguage($lang) {
    $this->language = $lang;
  }

}
