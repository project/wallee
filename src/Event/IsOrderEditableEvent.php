<?php

namespace Drupal\wallee\Event;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\EventDispatcher\Event;
use Drupal\commerce_order\Entity\Order as CommerceOrder;

/**
 * Wraps a node insertion demo event for event listeners.
 */
class IsOrderEditableEvent extends Event {

  protected $order;

  protected $editable;

  const IS_ORDER_EDITABLE = 'event_subscriber_wallee.commerce_order.is_editable';

  /**
   * Constructs a node insertion demo event object.
   *
   * @param EntityInterface $entity
   */
  public function __construct(CommerceOrder $order) {
    $this->order = $order;
    // $this->editable = $editable;.
  }

  /**
   * Get the inserted entity.
   *
   * @return EntityInterface
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   *
   */
  public function getEditable() {
    return $this->editable;
  }

  /**
   *
   */
  public function setEditable($is_editable) {
    $this->editable = $is_editable;
  }

}
