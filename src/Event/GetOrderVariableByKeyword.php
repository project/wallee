<?php

namespace Drupal\wallee\Event;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Wraps a node insertion demo event for event listeners.
 */
class GetOrderVariableByKeyword extends Event {

  protected $order;

  protected $variable_name;

  protected $variable_value;

  const ORDER_VARIABLE = 'event_subscriber_wallee.order.variable';

  /**
   * Constructs a node insertion demo event object.
   *
   * @param EntityInterface $entity
   */
  public function __construct($order, $keyword) {
    $this->order = $order;
    $this->variable_name = $keyword;
  }

  /**
   *
   */
  public function getVariableName() {
    return $this->variable_name;
  }

  /**
   *
   */
  public function getVariableValue() {
    return $this->variable_value;
  }

  /**
   *
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   *
   */
  public function setVariableValue($value) {
    $this->variable_value = $value;
  }

}
