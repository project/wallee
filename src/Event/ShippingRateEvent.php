<?php

namespace Drupal\wallee\Event;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\EventDispatcher\Event;
use Drupal\commerce_order\Entity\Order as CommerceOrder;

/**
 * Wraps a node insertion demo event for event listeners.
 */
class ShippingRateEvent extends Event {

  protected $order;

  protected $shipping_vat;

  const SHIPPING_VAT = 'event_subscriber_wallee.shipping.shipping_vat';

  /**
   * Constructs a node insertion demo event object.
   *
   * @param EntityInterface $entity
   */
  public function __construct(CommerceOrder $order) {
    $this->order = $order;
    // $this->editable = $editable;.
  }

  /**
   * Get the inserted entity.
   *
   * @return EntityInterface
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   *
   */
  public function getVat() {
    return $this->shipping_vat;
  }

  /**
   *
   */
  public function setVat($vat) {
    $this->shipping_vat = $vat;
  }

}
