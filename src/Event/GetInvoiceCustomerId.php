<?php

namespace Drupal\wallee\Event;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Wraps a node insertion demo event for event listeners.
 */
class GetInvoiceCustomerId extends Event {

  protected $order;

  protected $customer_id;

  const INVOICE_CUSTOMER_ID = 'event_subscriber_wallee.customer.id';

  /**
   * Constructs a node insertion demo event object.
   *
   * @param EntityInterface $entity
   */
  public function __construct($order) {
    $this->order = $order;
  }

  /**
   * Get the inserted entity.
   *
   * @return EntityInterface
   */
  public function getCustomerId() {
    return $this->customer_id;
  }

  /**
   *
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   *
   */
  public function setCustomerId($id) {
    $this->customer_id = $id;
  }

}
