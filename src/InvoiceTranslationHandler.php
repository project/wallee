<?php

namespace Drupal\wallee;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for invoice.
 */
class InvoiceTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
