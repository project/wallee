<?php

namespace Drupal\wallee\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class walleeSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wallee_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'wallee.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('wallee.settings');

    $form['wallee_spaceId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Space ID'),
      '#default_value' => $config->get('wallee_spaceId'),
    ];

    $form['wallee_appUserId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wallee Application User Id'),
      '#default_value' => $config->get('wallee_appUserId'),
    ];

    $form['wallee_appUserPwd'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wallee Application User Password'),
      '#default_value' => $config->get('wallee_appUserPwd'),
    ];

    /* wallee does not use webhook identities any more
    $form['wallee_webhook_identity_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Webhook Identity Key'),
      '#default_value' => $config->get('wallee_webhook_identity_key'),
    );
    */

    $form['wallee_customer_mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Customer Email'),
      '#description' => $this->t('In case the Order Customer has no email, currently wallee needs an email otherwise it crushes.'),
      '#default_value' => $config->get('wallee_customer_mail'),
      '#required' => TRUE,
    ];

    // TODO: user should select Workflow, then State or Transition for this workflow
    // $file_path = DRUPAL_ROOT . '/modules/custom/custom_commerce/custom_commerce.workflows.yml';
    // $file_contents = file_get_contents($file_path);
    // $ymldata = Yaml::parse($file_contents);
    $form['invoice_automation'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Automatic Invoice Creation'),
    ];

    $form['invoice_automation']['wallee_invoice_creation'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Create Invoice Automatically when Order state goes to :'),
      '#description' => $this->t('Comma separated state ids e.g. completed,pending,paid. Typically this should be the "completed" state.'),
      '#default_value' => $config->get('wallee_invoice_creation'),
    ];

    $form['invoice_automation']['wallee_invoice_creation_per_payment_method'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Create Invoice Automatically ONLY for the following Payment Gateways :'),
      '#description' => $this->t('Comma separated payment gateway ids e.g. invoice,credit_card_payment. Typically this should be the machine name of the Invoice payment Gateway configured in commerce.'),
      '#default_value' => $config->get('wallee_invoice_creation_per_payment_method'),
    ];

    $form['invoice_automation']['wallee_invoice_state'] = [
      '#type' => 'textfield',
      '#title' => $this->t('When Invoice state changes, trigger an Order state change'),
      '#description' => $this->t('Comma separated state ids e.g. INVOICE_NEW_STATE,DRUPAL_ORDER_NEW_STATE'),
      '#default_value' => $config->get('wallee_invoice_state'),
    ];

    $form['payment_methods'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Payment Methods'),
      '#description' => "<img src='/" . drupal_get_path('module', 'wallee') . "/images/wallee_payment_methods.jpg'>",
    ];
    $form['payment_methods']['invoice_payment_method'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wallee Invoice Payment Method ID'),
      '#default_value' => $config->get('invoice_payment_method'),
    ];

    $form['payment_methods']['credit_card_payment_method'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Credit Card Payment Method ID'),
      '#default_value' => $config->get('credit_card_payment_method'),
    ];

    $form['payment_methods']['success_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment Success URL'),
      '#default_value' => $config->get('success_url'),
    ];

    $form['payment_methods']['fail_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment Failure URL'),
      '#default_value' => $config->get('fail_url'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable('wallee.settings')
      // Set the submitted configuration setting.
      ->set('wallee_spaceId', $form_state->getValue('wallee_spaceId'))
      ->set('wallee_appUserId', $form_state->getValue('wallee_appUserId'))
      ->set('wallee_appUserPwd', $form_state->getValue('wallee_appUserPwd'))
      ->set('wallee_customer_mail', $form_state->getValue('wallee_customer_mail'))
      ->set('wallee_invoice_creation', $form_state->getValue('wallee_invoice_creation'))
      ->set('wallee_invoice_state', $form_state->getValue('wallee_invoice_state'))
      ->set('invoice_payment_method', $form_state->getValue('invoice_payment_method'))
      ->set('credit_card_payment_method', $form_state->getValue('credit_card_payment_method'))
      ->set('success_url', $form_state->getValue('success_url'))
      ->set('fail_url', $form_state->getValue('fail_url'))
      ->set('wallee_invoice_creation_per_payment_method', $form_state->getValue('wallee_invoice_creation_per_payment_method'))
      // ->set('wallee_webhook_identity_key', $form_state->getValue('wallee_webhook_identity_key'))

      ->save();

    parent::submitForm($form, $form_state);
  }

}
