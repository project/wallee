<?php

namespace Drupal\wallee\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Invoice entities.
 *
 * @ingroup wallee
 */
class InvoiceDeleteForm extends ContentEntityDeleteForm {


}
