<?php

namespace Drupal\wallee\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\wallee\Entity\InvoiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Invoice revision.
 *
 * @ingroup wallee
 */
class InvoiceRevisionDeleteForm extends ConfirmFormBase {


  /**
   * The Invoice revision.
   *
   * @var InvoiceInterface
   */
  protected $revision;

  /**
   * The Invoice storage.
   *
   * @var EntityStorageInterface
   */
  protected $InvoiceStorage;

  /**
   * The database connection.
   *
   * @var Connection
   */
  protected $connection;

  /**
   * Constructs a new InvoiceRevisionDeleteForm.
   *
   * @param EntityStorageInterface $entity_storage
   *   The entity storage.
   * @param Connection $connection
   *   The database connection.
   */
  public function __construct(EntityStorageInterface $entity_storage, Connection $connection) {
    $this->InvoiceStorage = $entity_storage;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity.manager');
    return new static(
      $entity_manager->getStorage('invoice'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'invoice_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to delete the revision from %revision-date?', ['%revision-date' => format_date($this->revision->getRevisionCreationTime())]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.invoice.version_history', ['invoice' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $invoice_revision = NULL) {
    $this->revision = $this->InvoiceStorage->loadRevision($invoice_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->InvoiceStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')
      ->notice('Invoice: deleted %title revision %revision.', [
        '%title' => $this->revision->label(),
        '%revision' => $this->revision->getRevisionId(),
      ]);
    drupal_set_message(t('Revision from %revision-date of Invoice %title has been deleted.', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
      '%title' => $this->revision->label(),
    ]));
    $form_state->setRedirect(
      'entity.invoice.canonical',
      ['invoice' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {invoice_field_revision} WHERE id = :id', [':id' => $this->revision->id()])
        ->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.invoice.version_history',
        ['invoice' => $this->revision->id()]
      );
    }
  }

}
