<?php

namespace Drupal\wallee;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\wallee\Entity\Invoice;

/**
 * Defines a class to build a listing of Invoice entities.
 *
 * @ingroup wallee
 */
class InvoiceListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Invoice ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity Invoice */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.invoice.edit_form',
      ['invoice' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
