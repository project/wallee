<?php

namespace Drupal\wallee\Controller;

use Drupal;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\wallee\Event\IsOrderEditableEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\wallee\WalleeService;
use Drupal\wallee\Entity\Invoice;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * An example controller.
 */
class WalleeController extends ControllerBase {

  protected $walleeService;

  protected $configFactory;

  protected $entityTypeManager;

  /**
   *
   */
  public function __construct(WalleeService $walleeService, EntityTypeManagerInterface $entityTypeManager, ConfigFactoryInterface $config_factory) {
    $this->walleeService = $walleeService;
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('wallee.wallee_service'), $container->get("entity_type.manager"),
      $container->get('config.factory')
    );
  }

  /**
   *
   */
  public function invoiceReady(Request $request) {
    // Sample webhook request for invoice ready requres.
    // @"eventId":1948763,"entityId":310140,"listenerEntityId":1472041816898,"listenerEntityTechnicalName":"TransactionInvoice","state":"NOT_APPLICABLE","spaceId":1359,"webhookListenerId":3151,"timestamp":"2018-10-12T06:34:56+0000"
    $params = [];
    $content = $request->getContent();

    Drupal::logger('wallee')->notice("webhook authentication success!");

    if (!empty($content)) {
      $params = json_decode($content, TRUE);
    }
    Drupal::logger('wallee')
      ->notice("invoice ready webhook for invoice with wallee id:" . $params["entityId"]);
    $invoice_id = $params["entityId"];

    $invoice = $this->walleeService->getWalleeInvoiceByWalleeInvoiceId($invoice_id);
    $invoice_state = $invoice->getState();

    if ($invoice_state == "NOT_APPLICABLE") {
      return new Response("");
    }

    // \Drupal::logger('wallee')->notice($content);
    return new Response($this->walleeService->invoiceStateChanged($invoice_id, $invoice_state));

  }

  /**
   *
   */
  public function paymentAttempted(Request $request) {
    // Sample webhook request for charge attempt.
    // @"eventId":1948762,"entityId":435321,"listenerEntityId":1472041801860,"listenerEntityTechnicalName":"ChargeAttempt","state":"SUCCESSFUL","spaceId":1359,"webhookListenerId":4490,"timestamp":"2018-10-12T06:34:54+0000"
    $params = [];
    $content = $request->getContent();
    Drupal::logger('wallee')->notice($content);

    if (!empty($content)) {
      $params = json_decode($content, TRUE);
    }

    if (!isset($params["listenerEntityTechnicalName"]) || $params["listenerEntityTechnicalName"] !== "ChargeAttempt") {
      return;
    }

    $payment_entity_id = $params["entityId"];

    if ($params["state"] == "SUCCESSFUL") {
      return new Response($this->walleeService->handleSuccessfulPayment($payment_entity_id));
      // if($params["stage"] == "FAILED")
    }
    else {
      return new Response($this->walleeService->handleFailedPayment($payment_entity_id));
    }
  }

  /**
   *
   */
  public function createTransaction($order_id) {
    $ret = $this->walleeService->createTransactionFromOrderId($order_id);

    $return_path = "/admin/commerce/orders/" . $order_id;
    return new RedirectResponse($_SERVER["HTTP_REFERER"]);
  }

  /**
   *
   */
  public function invoicesPage(OrderInterface $commerce_order) {
    // Return $this->walleeService->invoiceList($commerce_order->id());
    // check if this order even has the correct wallee invoice field.
    $invoice_field_name = $this->walleeService->getWalleeInvoiceFieldNameByOrder($commerce_order);
    if ($invoice_field_name == "") {
      $orderTypeName = $commerce_order->get("type")->getValue()[0]["target_id"];
      $order_type_label = $this->entityTypeManager->getStorage('commerce_order_type')
        ->load($orderTypeName)
        ->label();

      return [
        '#markup' => '<p>The Order Type "' . $order_type_label . '" does not have the "Wallee Invoice Reference" field. 
        This field is mandatory in order to integrate the Order Type with the Wallee invoice functionality.</p><p>Please add this field to the Order Type.</p>',
      ];
    }

    // $order = $this->orderManager->load($orderID);
    $invoice_ids = $this->walleeService->getInvoiceEntityByOrderId($commerce_order->id());

    $wallee_transactions_array = [];

    if (gettype($invoice_ids) == "array") {
      for ($i = 0; $i < count($invoice_ids); $i++) {
        $invoice_entity = Invoice::load($invoice_ids[$i]);
        $fid = $invoice_entity->get("invoice_file")->getValue()[0]["target_id"];

        $wallee_transactions_array[$i]["invoice_file_url"] = "";
        if ($fid > 0) {
          $file = $this->entityTypeManager->getStorage('file')->load($fid);
          $uri = $file->uri;
          $url = file_create_url($uri->getValue()[0]["value"]);
          $wallee_transactions_array[$i]["invoice_file_url"] = $url;
        }

        $wallee_transactions_array[$i]["transaction_timestamp"] = date('d-m-Y h:m:s', $invoice_entity->getCreatedTime());
        $wallee_transactions_array[$i]["invoice_id"] = $invoice_ids[$i];
        if (count($invoice_entity->get("state")->getValue()) > 0) {
          $wallee_transactions_array[$i]["invoice_state"] = $invoice_entity->get("state")
            ->getValue()[0]["value"];
        }

        $wallee_transactions_array[$i]["transaction_id"] = $invoice_entity->get("transaction_id")
          ->getValue()[0]["value"];
      }
    }

    // Dispatch an IS_ORDER_EDITABLE event, in case any module needs to update this with custom logic.
    $event = Drupal::service('event_dispatcher')
      ->dispatch(IsOrderEditableEvent::IS_ORDER_EDITABLE, new IsOrderEditableEvent($commerce_order));

    $is_editable = TRUE;
    if ($event->getEditable() == FALSE) {
      $is_editable = FALSE;
    }
    // $payment_gateway = $commerce_order->get("payment_gateway")->getValue()[0]["target_id"];
    $payment_gateway = $commerce_order->get('payment_gateway')->getEntity();
    if ($payment_gateway != NULL) {
      $payment_gateway_label = $commerce_order->get('payment_gateway')
        ->getValue()[0]["target_id"];

      // If order's payment gateway is credit card, then no invoice creation should be allowed.
      $paymentPluginId = $this->walleeService->getOrderPaymentGatewayPluginId($commerce_order);

      if ($paymentPluginId == "wallee_offsite_redirect") {
        return [
          '#markup' => '<p>The current payment method is ' . $payment_gateway_label . '. This payment method does not support invoicing.</p>',
        ];
      }
    }

    // Check if invoice file exists, otherwise we need to show the button.
    if (!$this->walleeService->verifyInvoiceExists($commerce_order)) {
      $is_editable = TRUE;
    }

    return [
      '#theme' => 'invoice_template',
      '#invoices' => $wallee_transactions_array,
      '#order_id' => $commerce_order->id(),
      '#is_order_editable' => $is_editable,
      '#cache' => ['max-age' => 0],
    ];

  }

  /**
   *
   */
  public function getInvoice($transaction_id) {
    /*
    $invoiceData = $this->walleeService->getInvoicesByTransactionId($transaction_id);

    header("Content-type: ".$invoiceData->getMimeType());
    header("Content-disposition: attachment;filename=".$invoiceData->getTitle().".pdf");
    $content = $invoiceData->getData();

    return new Response(base64_decode($content));
     */
  }

  /*
  public function syncLocalInvoiceWithWallee($order_id){
  $this->walleeService->syncLocalInvoiceWithWallee($order_id);

  //$return_url = URL::fromRoute("entity.wallee_invoice.collection", array("order_id"=>64));
  //return new RedirectResponse($return_url);
  //return  new RedirectResponse($return_url->getRouteName());

  return new RedirectResponse("/admin/commerce/orders/".$order_id."/wallee_invoices");
  }

  public function updateLocalInvoiceStateByOrder($order_id){
  $this->walleeService->updateLocalInvoiceState($order_id);

  //$return_url = URL::fromRoute("entity.wallee_invoice.collection", array("order_id"=>64));
  //return new RedirectResponse($return_url);
  //return  new RedirectResponse($return_url->getRouteName());

  return new RedirectResponse("/admin/commerce/orders/".$order_id."/wallee_invoices");
  }
   */
}
