<?php

namespace Drupal\wallee\Controller;

use Drupal;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\wallee\Entity\InvoiceInterface;

/**
 * Class InvoiceController.
 *
 *  Returns responses for Invoice routes.
 */
class InvoiceController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Invoice  revision.
   *
   * @param int $invoice_revision
   *   The Invoice  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($invoice_revision) {
    $invoice = $this->entityTypeManager->getStorage('invoice')
      ->loadRevision($invoice_revision);
    $view_builder = $this->entityTypeManager->getViewBuilder('invoice');

    return $view_builder->view($invoice);
  }

  /**
   * Page title callback for a Invoice  revision.
   *
   * @param int $invoice_revision
   *   The Invoice  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($invoice_revision) {
    $invoice = $this->entityTypeManager->getStorage('invoice')
      ->loadRevision($invoice_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $invoice->label(),
      '%date' => format_date($invoice->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Invoice .
   *
   * @param InvoiceInterface $invoice
   *   A Invoice  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(InvoiceInterface $invoice) {
    $account = $this->currentUser();
    $langcode = $invoice->language()->getId();
    $langname = $invoice->language()->getName();
    $languages = $invoice->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $invoice_storage = $this->entityTypeManager->getStorage('invoice');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', [
      '@langname' => $langname,
      '%title' => $invoice->label(),
    ]) : $this->t('Revisions for %title', ['%title' => $invoice->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all invoice revisions") || $account->hasPermission('administer invoice entities')));
    $delete_permission = (($account->hasPermission("delete all invoice revisions") || $account->hasPermission('administer invoice entities')));

    $rows = [];

    $vids = $invoice_storage->revisionIds($invoice);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      $revision = $invoice_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)
          ->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = Drupal::service('date.formatter')
          ->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $invoice->getRevisionId()) {
          $link = $this->l($date, new Url('entity.invoice.revision', [
            'invoice' => $invoice->id(),
            'invoice_revision' => $vid,
          ]));
        }
        else {
          $link = $invoice->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => Drupal::service('renderer')->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
                Url::fromRoute('entity.invoice.translation_revert', [
                  'invoice' => $invoice->id(),
                  'invoice_revision' => $vid,
                  'langcode' => $langcode,
                ]) :
                Url::fromRoute('entity.invoice.revision_revert', [
                  'invoice' => $invoice->id(),
                  'invoice_revision' => $vid,
                ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.invoice.revision_delete', [
                'invoice' => $invoice->id(),
                'invoice_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['invoice_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
