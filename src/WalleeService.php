<?php

namespace Drupal\wallee;

use Drupal;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\file\Entity\File;
use Wallee\Sdk\Model\TransactionCreate;
use Wallee\Sdk\Model\LineItemCreate;
use Wallee\Sdk\Model\AddressCreate;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\wallee\Entity\Invoice;
use Drupal\Core\Messenger\MessengerInterface;
use Wallee\Sdk\Model\TaxCreate;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\wallee\Event\GetUserLanguageEvent;
use Drupal\wallee\Event\GetOrderVariableByKeyword;
use Drupal\Core\Site\Settings;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\wallee\Event\ShippingRateEvent;
use Drupal\wallee\Event\GetInvoiceCustomerId;
use Drupal\Core\Queue\QueueFactory;
use Drupal\commerce_order\Entity\OrderItem;
/**
 *
 */
class WalleeService {

  use StringTranslationTrait;

  const PROCESS_IMMEDIATE = 'immediate';

  const PROCESS_QUEUE = 'queue';

  protected $spaceId;

  protected $appUserId;

  protected $appUserPwd;

  protected $walleeWrapper;

  protected $config;

  protected $orderManager;

  protected $orderItemManager;

  protected $profileManager;

  protected $invoiceManager;

  protected $apiClient;

  protected $languageManager;

  protected $entityTypeManager;

  protected $messenger;

  protected $settings;

  protected $loggerFactory;

  protected $queueFactory;

  /**
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Language\LanguageManagerInterface $lang_manager
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * @param \Drupal\Core\Site\Settings $settings
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, LanguageManagerInterface $lang_manager, MessengerInterface $messenger, Settings $settings, LoggerChannelFactoryInterface $loggerChannelFactory, QueueFactory $queue_factory) {

    $this->config = $config_factory->get('wallee.settings');
    $this->settings = $settings;

    $this->walleeWrapper = new WalleeWrapper($this->config, $messenger);

    $this->entityTypeManager = $entity_type_manager;
    $this->orderManager = $entity_type_manager->getStorage("commerce_order");
    $this->orderItemManager = $entity_type_manager->getStorage('commerce_order_item');
    $this->profileManager = $entity_type_manager->getStorage('profile');
    $this->invoiceManager = $entity_type_manager->getStorage('invoice');
    $this->languageManager = $lang_manager;
    $this->messenger = $messenger;
    $this->loggerFactory = $loggerChannelFactory->get("wallee");
    $this->queueFactory = $queue_factory;
  }

  /**
   * @param Invoice $invoice
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onInvoiceDelete(Invoice $invoice) {
    // If an invoice entity is deleted, make sure we also delete the actual file entity.
    $fid = $invoice->get("invoice_file")->getValue()[0]["target_id"];
    if ($fid != NULL) {
      $file_obj = $this->entityTypeManager->getStorage("file")->load($fid);
      $file_obj->delete();
    }
  }

  /**
   * Processes via cron queues or immediately.
   *
   * @param int $order_id
   * @param string $type
   *
   *   * @return integer
   *   The transaction id.
   */
  public function createTransactionFromOrderId(int $order_id, $type = WalleeService::PROCESS_IMMEDIATE) {
    return $this->createTransactionFromOrderIdExecute($order_id);
  }

  /**
   * @param $order_id
   *
   * @return int|string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createTransactionFromOrderIdExecute(int $order_id) {
    $transaction_id = "";
    $order = $this->orderManager->load($order_id);
    $lineItems = $this->getLineItemsFromOrder($order);
    if (count($lineItems) == 0) {
      $this->messenger->addMessage(t('There are no order items in this order'));
      return 0;
    }
    $currency = $order->getTotalPrice()->getCurrencyCode();
    $customer_email = $order->getEmail();
    $customerID = $order->getCustomer()->id();
    $lang = $this->getCustomerLanguage($customerID);

    // Check if we have extra fields in the order, marked as Wallee Field
    // so that we include them in the Wallee request.
    $wallee_meta = [];
    $extra_invoice_fields = $this->getExtraInvoiceFields($order);
    if (count($extra_invoice_fields) > 0 && isset($extra_invoice_fields[0])) {
      foreach ($extra_invoice_fields as $extra_invoice_field) {
        if (isset($order->get($extra_invoice_field)->getValue()[0])) {
          $value_of_extra = $order->get($extra_invoice_field)
            ->getValue()[0]["value"];
          if ($value_of_extra != "") {
            $wallee_meta[$extra_invoice_field] = $value_of_extra;
          }
        }
      }
    }

    // Use wallee php-sdk to create the transaction.
    $transaction = new TransactionCreate();
    $transaction->setCurrency($currency);
    $transaction->setLineItems($lineItems);

    if ($customer_email == "") {
      $customer_email = $this->config->get('wallee_customer_mail');
    }

    $transaction->setCustomerEmailAddress($customer_email);
    $transaction->setCustomerId($this->getInvoiceCustomerId($order));
    $transaction->setMerchantReference($order->id());
    $transaction->setLanguage($lang . "-CH");

    // These are the success and failed urls that the user will be redirected to after payment
    // the actual order state is going to be changed after a wallee webhook contacts us with the
    // new transaction state, this is done asynchronously via webhook.
    $transaction->setSuccessUrl($this->config->get('success_url'));
    $transaction->setFailedUrl($this->config->get('fail_url'));

    // Set the correct payment method.
    $this->setPaymentMethod($order, $transaction);

    $billing_profile_entity = $order->getBillingProfile();

    if (isset($billing_profile_entity)) {
      $billing_profile_id = $billing_profile_entity->get("profile_id")->value;
      $billing_profile = $this->profileManager->load($billing_profile_id);
      $city = $billing_profile->get("address")[0]->get("locality")->getValue();
      $postCode = $billing_profile->get("address")[0]->get("postal_code")
        ->getValue();
      $name = $billing_profile->get("address")[0]->get("given_name")
        ->getValue();
      $surname = $billing_profile->get("address")[0]->get("family_name")
        ->getValue();
      $organization = $billing_profile->get("address")[0]->get("organization")
        ->getValue();
      if ($organization == NULL) {
        $organization = $this->getAlternativeValueForKeyword($order, "organization");
      }
      $street = $billing_profile->get("address")[0]->get("address_line1")
        ->getValue();
      $country = $billing_profile->get("address")[0]->get("country_code")
        ->getValue();

      //$salutation_label = "";

      $address = new AddressCreate();

      $address->setCountry($country);
      $address->setCity($city);
      $address->setFamilyName($surname);
      $address->setGivenName($name);

      $address->setOrganizationName($organization);
      // $address->setGender($title_adr_label);
      $address->setPostCode($postCode);
      //$address->setSalutation($salutation_label);
      $address->setStreet($street);

      $transaction->setBillingAddress($address);

      if (count($wallee_meta) > 0 && $wallee_meta != "") {
        $transaction->setMetaData($wallee_meta);
      }
    }

    $return = "";
    try {
      Drupal::logger('wallee')
        ->notice("will try to create wallee transaction for order:" . $order_id);
      $transaction_id = $this->walleeWrapper->createTransaction($transaction);
      if (is_numeric($transaction_id) && $transaction_id > 0) {
        Drupal::logger('wallee')
          ->notice("wallee transaction with id:" . $transaction_id . " was created for order:" . $order_id);
        // If we dont need an invoice, nothing else to do here, bail out.
        $needInvoice = $this->doWeNeedInvoice($order);
        if (!$needInvoice) {
          Drupal::logger('wallee')
            ->notice("we DONT need to create an invoice for order:" . $order_id);
          return $transaction_id;
        }

        Drupal::logger('wallee')
          ->notice("we need to create an invoice for order:" . $order_id);

        // If we do need an invoice..
        // first delete all existing invoices that have a reference to this particular order, if there are.
        $order_invoices = $this->getInvoiceByOrderId($order_id);
        if (count($order_invoices) > 0) {
          foreach ($order_invoices as $order_invoice) {
            Drupal::logger('wallee')
              ->notice("delete existing invoice:" . $order_invoice->id() . " for order:" . $order_id);
            $order_invoice->delete();
          }
        }
        // We just issued the invoice creation
        // as this is an asynchronous action, there is a hook in wallee which will
        // notify us as soon as the invoice is ready.
        $empty_invoice = $this->createEmptyInvoice($order_id, $transaction_id);
        Drupal::logger('wallee')
          ->notice("created drupal empty invoice entity with id:" . $empty_invoice->id() . " for order:" . $order_id);
        $inv_entity_id = $empty_invoice->id();

        $wallee_invoice_field_name = $this->getWalleeInvoiceFieldNameByOrder($order);
        Drupal::logger('wallee')
          ->notice("wallee invoice field name:" . $wallee_invoice_field_name);

        // Now we need to find the order which uses the above invoice and make the reference.
        if ($wallee_invoice_field_name !== "") {
          // $order->$wallee_invoice_field_name[] = ['invoice_id' => $inv_entity_id];.
          $order->set($wallee_invoice_field_name, $inv_entity_id);
          $order->save();
        }
      }
    } catch (Exception $e) {
      echo 'Exception when calling AccountService->count: ', $e->getMessage(), PHP_EOL;
    }

    return $transaction_id;
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return array|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getLineItemsFromOrder(OrderInterface $order) {
    $lineItems = [];

    $order_items = $order->get("order_items")->getValue();

    // If order has no items, exit.
    if (count($order_items) == 0) {
      return NULL;
    }

    $shipping = 0;
    $discount = 0;
    // For every drupal commerce order item, we need to create a wallee transaction order item.
    foreach ($order_items as $key => $value) {
      Drupal::logger('wallee')
        ->notice("order:" . $order->id() . " order item key:" . $key . " , order item value:" . implode(",", $value));
      $target_id = $value["target_id"];
      $order_item = $this->orderItemManager->load($target_id);
      if ($order_item == NULL) {
        Drupal::logger('wallee')
          ->notice("could not load order item for order:" . $order->id() . " with target_id:" . $target_id);
        continue;
      }

      $discount += $this->getOrderItemDiscount($order_item);
      $orderItemInfo = $this->getInfoOutOfOrderItem($order_item);

      // Create and set values to wallee line item.
      $lineItem = new LineItemCreate();
      $this->prepareWalleeLineItem($lineItem, $orderItemInfo);
      $lineItems[] = $lineItem;
    }

    // If there is discount, we need to create one more Order Item, of type DISCOUNT.
    if ($discount != 0) {
      $lineItem = new LineItemCreate();
      $lineItem->setName("Discount");
      $lineItem->setQuantity(1);
      // Just add a fixed dummy sku.
      $lineItem->setSku("DISCOUNT1234");
      $lineItem->setUniqueId("DISCOUNT1234");
      $lineItem->setAmountIncludingTax($discount);

      $lineItem->setType("DISCOUNT");
      $lineItems[] = $lineItem;
    }

    $shipping = $this->getShippingByOrder($order);
    // If there is shipping, we need to create one more Order Item, of type SHIPPING.
    if ($shipping > 0) {
      // Dispatch an SHIPPING_RATE event, in case any module needs to update this with custom logic.
      $event = Drupal::service('event_dispatcher')
        ->dispatch(ShippingRateEvent::SHIPPING_VAT, new ShippingRateEvent($order));
      $shipping_tax = $event->getVat();

      // if($shipping_tax > 0)
      //  $shipping = $shipping*$shipping_tax;.
      $customer_lang = $this->getCustomerLanguage($order->getCustomerId());

      $lineItem = new LineItemCreate();
      $shipping_title = $this->t("Shipping", [], ['langcode' => $customer_lang])
        ->render();
      $lineItem->setName($shipping_title);
      $lineItem->setQuantity(1);
      // Just add a fixed dummy sku.
      $lineItem->setSku("SHIPPING1234");
      $lineItem->setUniqueId("SHIPPING1234");
      $lineItem->setAmountIncludingTax(round($shipping, 2));

      if ($shipping_tax != NULL) {
        $lineItem->setTaxes(new TaxCreate([
          "rate" => $shipping_tax,
          "title" => "Shipping Tax",
        ]));
      }

      $lineItem->setType("SHIPPING");
      $lineItems[] = $lineItem;
    }

    return $lineItems;
  }

  /**
   * @param OrderItem $order_item
   *
   * @return int
   */
  private function getOrderItemDiscount(OrderItem $order_item) {
    $adjs = $order_item->getAdjustments();
    if (count($adjs) > 0) {
      foreach ($adjs as $adj) {
        if ($adj->getType() == "promotion") {
          return $adj->getAmount()->getNumber();
        }
      }
    }
    return 0;
  }

  /**
   * @param OrderItem $order_item
   *
   * @return array|mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getInfoOutOfOrderItem(OrderItem $order_item) {
    $ret = [];
    $ret = $this->getInfoArrayOutOfOrderItem($order_item);

    $tax = $this->getTaxByOrderItem($order_item);
    if ($tax != "") {
      $ret["tax"] = $tax;
    }

    return $ret;
  }

  /**
   * @param OrderItem $order_item
   *
   * @return array|mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getInfoArrayOutOfOrderItem(OrderItem $order_item) {
    $ret = [];
    $product = $order_item->getPurchasedEntity();

    // The only case where we might have an Order Item out of a non product, is the donation.
    // as donation is implemented (https://github.com/bojanz/donation_example)
    // it is only an Order Item, but not a product.
    if (!$product) {
      $ret = $this->getInfoArrayOutOfDonationOrderItem($order_item);
    }
    else {
      $ret = $this->getInfoArrayOutOfProduct($order_item);
    }

    return $ret;
  }

  /**
   * @param $order_item
   *
   * @return mixed
   */
  public function getInfoArrayOutOfDonationOrderItem($order_item) {
    // Get values from drupal order item.
    $ret["title"] = $order_item->getTitle();
    $ret["sku"] = "NO-SKU" . rand();
    $ret["price"] = $order_item->getTotalPrice()->getNumber();
    $ret["currency"] = $order_item->getTotalPrice()->getCurrencyCode();
    $ret["quantity"] = $order_item->getQuantity();
    $ret["total_price"] = round($order_item->getUnitPrice()->getNumber(), 2);
    $ret["item_id"] = $order_item->id();

    return $ret;
  }

  /**
   * @param OrderItem $order_item
   *
   * @return mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getInfoArrayOutOfProduct(OrderItem $order_item) {
    $product = $order_item->getPurchasedEntity();
    $customer_id = $order_item->getOrder()
      ->get("uid")
      ->getValue()[0]["target_id"];

    $user_lang = $this->getCustomerLanguage($customer_id);

    // Get values from drupal Product
    // so far we only care about the translated title, we assume price, vat etc are the same for all translations of the same product.
    if ($product->isTranslatable() && $product->hasTranslation($user_lang)) {
      $ret["title"] = $product->getTranslation($user_lang)->getTitle();
    }
    else {
      $ret["title"] = $product->getTitle();
    }

    $ret["sku"] = $product->getSku();
    $ret["price"] = $product->getPrice()->getNumber();
    $ret["currency"] = $product->getPrice()->getCurrencyCode();
    $ret["quantity"] = $order_item->getQuantity();
    $ret["total_price"] = round($order_item->getUnitPrice()->getNumber(), 2);
    $ret["item_id"] = $order_item->id();

    return $ret;
  }

  /**
   * @param integer $contact_id
   *
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getCustomerLanguage(int $contact_id) {
    $customer = $this->entityTypeManager->getStorage("user")->load($contact_id);

    // Dispatch an USER_PREFERRED_LANG event, in case any module needs to update this with custom logic.
    $event = Drupal::service('event_dispatcher')
      ->dispatch(GetUserLanguageEvent::USER_PREFERRED_LANG, new GetUserLanguageEvent($customer));

    $user_lang = $event->getLanguage();
    if (!isset($user_lang) || $user_lang == "") {
      $user_lang = $customer->getPreferredLangcode();
    }

    return $user_lang;
  }

  /**
   * @param OrderItem $order_item
   * @return string|\Wallee\Sdk\Model\TaxCreate
   */
  private function getTaxByOrderItem(OrderItem $order_item) {
    $tax = "";
    $adjs = $order_item->getAdjustments();
    $vat_perc = 0;
    $vat_title = "NO VAT";
    if (count($adjs) > 0) {
      foreach ($adjs as $adj) {
        $type = $adj->getType();
        // Set the Tax amounts only if the order has been paid.
        if ($type == 'tax') {
          $vat_perc = $adj->getPercentage() * 100;
          $vat_title = $adj->getLabel();
        }
      }
      $tax = new TaxCreate(["rate" => $vat_perc, "title" => $vat_title]);
    }
    return $tax;
  }

  /**
   * @param LineItemCreate $lineItem
   * @param array $orderItemInfo
   */
  private function prepareWalleeLineItem(LineItemCreate &$lineItem, array $orderItemInfo) {
    $lineItem->setName($orderItemInfo["title"]);
    $lineItem->setQuantity($orderItemInfo["quantity"]);
    $lineItem->setSku($orderItemInfo["sku"]);
    $lineItem->setUniqueId($orderItemInfo["item_id"]);
    $lineItem->setAmountIncludingTax($orderItemInfo["total_price"] * $orderItemInfo["quantity"]);
    if (isset($orderItemInfo["tax"])) {
      $lineItem->setTaxes($orderItemInfo["tax"]);
    }

    $lineItem->setType("PRODUCT");
  }

  /**
   * Try to figure out if shipping adjustment exist
   * @param OrderInterface $order
   * @return string
   */
  private function getShippingByOrder(OrderInterface $order) {
    $shipping = "";
    $adjs = $order->getAdjustments();
    if (count($adjs) > 0) {
      foreach ($adjs as $adj) {
        $type = $adj->getType();
        //if there is an order adjustment with 'shipping' contained in the type name
        //get the amount
        if (strpos($type, "shipping") >= 0) {
          $shipping = $adj->getAmount()->getNumber();
        }
      }
    }
    return $shipping;
  }

  /**
   * @param OrderInterface $order
   * @return array
   */
  public function getExtraInvoiceFields(OrderInterface $order) {
    $field_names = [];

    foreach ($order->getFieldDefinitions() as $fieldDefinition) {
      // If this order type contains our Wallee Field Type, then update it accordingly.
      if (method_exists($fieldDefinition, "getThirdPartySettings")) {
        $third_party = $fieldDefinition->getThirdPartySettings("wallee", "add_to_wallee_invoice");
        if (count($third_party) == 1) {
          if ($fieldDefinition->get("field_name") != "") {
            $field_names[] = $fieldDefinition->get("field_name");
          }
        }
      }
    }
    return $field_names;
  }

  /**
   * @param OrderInterface $order
   * @return integer
   */
  private function getInvoiceCustomerId(OrderInterface $order) {
    // Default customer id is the order customer id.
    $customer_id = $order->getCustomer()->id();

    // however, some other module might have a different opinion about this, so lets ask
    // dispatch an INVOICE_CUSTOMER_ID event, in case any module needs to update this with custom logic.
    $event = Drupal::service('event_dispatcher')
      ->dispatch(GetInvoiceCustomerId::INVOICE_CUSTOMER_ID, new GetInvoiceCustomerId($order));

    if ($event->getCustomerId() != NULL) {
      return $event->getCustomerId();
    }

    return $customer_id;
  }

  /**
   * @param OrderInterface $order
   * @param TransactionCreate $transaction
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function setPaymentMethod(OrderInterface $order, TransactionCreate &$transaction) {
    $invoice_pm = $this->config->get('invoice_payment_method');
    $credit_card_pm = $this->config->get('credit_card_payment_method');

    $paymentPluginId = $this->getOrderPaymentGatewayPluginId($order);
    // If the order payment method is Credit Card, we set that.
    if ($paymentPluginId == "wallee_offsite_redirect" && $credit_card_pm != "") {
      $transaction->setAllowedPaymentMethodConfigurations($credit_card_pm);
    }
    elseif ($invoice_pm != "") {
      $transaction->setAllowedPaymentMethodConfigurations($invoice_pm);
    }
    else {
      return;
    }

  }

  /**
   * @param OrderInterface $order
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getOrderPaymentGatewayPluginId(OrderInterface $order) {
    if ($order == NULL) {
      return "";
    }

    if (count($order->get("payment_gateway")->getValue()) == 0) {
      return "";
    }
    $payment_gateway_id = $order->get("payment_gateway")
      ->getValue()[0]["target_id"];
    $payment_gateway = $this->entityTypeManager->getStorage("commerce_payment_gateway")
      ->load($payment_gateway_id);
    if ($payment_gateway != NULL) {
      return $payment_gateway->getPlugin()->getPluginId();
    }

    return "";
  }

  /**
   * Dispatch an ORDER_VARIABLE event, in case any module needs to update this
   * with custom logic.
   * @param OrderInterface $order
   * @param string $keyword
   * @return string
   */
  private function getAlternativeValueForKeyword(OrderInterface $order, string $keyword) {
    $event = Drupal::service('event_dispatcher')
      ->dispatch(GetOrderVariableByKeyword::ORDER_VARIABLE, new GetOrderVariableByKeyword($order, $keyword));

    return $event->getVariableValue();
  }

  /**
   * @param OrderInterface $order
   * @return bool
   */
  public function doWeNeedInvoice(OrderInterface $order) {
    // Out of convention, an invoice payment needs an invoice
    // but a credit card payment does not.
    $paymentPluginId = $this->getOrderPaymentGatewayPluginId($order);
    if ($paymentPluginId == "wallee_offsite_redirect") {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * @param int $order_id
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  public function getInvoiceByOrderId(int $order_id) {
    $invoice = $this->invoiceManager->loadByProperties(['order_id' => $order_id]);
    return $invoice;
  }

  /**
   * @param int $order_id
   * @param int $transaction_id
   * @return \Drupal\Core\Entity\EntityInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createEmptyInvoice(int $order_id, int $transaction_id) {
    $order = $this->entityTypeManager->getStorage("commerce_order")
      ->load($order_id);
    $customer_id = $order->get("uid")->getValue()[0]["target_id"];
    $lang = $this->getCustomerLanguage($customer_id);

    // Create the invoice entity.
    $invoiceObj = Invoice::create(
      [
        'order_id' => $order_id,
        'transaction_id' => $transaction_id,
        'state' => "WAITING_WALLEE",
        'langcode' => $lang,
      ]
    );

    // Save the invoice.
    $invoiceObj->save();
    return $invoiceObj;
  }

  /**
   * @param OrderInterface $order
   * @return string
   */
  public function getWalleeInvoiceFieldNameByOrder(OrderInterface $order) {
    $field_name = "";

    foreach ($order->getFieldDefinitions() as $fieldDefinition) {
      // If this order type contains our Wallee Field Type, then update it accordingly.
      if ($fieldDefinition->getType() == "field_wallee_invoice_ref") {
        $field_name = $fieldDefinition->getName();
      }
    }
    return $field_name;
  }

  /**
   * @param integer $order_id
   * @return integer|null
   */
  public function getPaymentGatewaysByOrderId(int $order_id) {
    $order = $this->orderManager->load($order_id);
    if (count($order->get("payment_gateway")->getValue()) != 1) {
      return NULL;
    }

    return $order->get("payment_gateway")->getValue()[0]["target_id"];
  }

  /**
   * Update state of local invoices, if the current state is OPEN (default
   * filter)
   * @param int $order_id
   * @param array $invoice_state
   */
  public function updateLocalInvoiceState(int $order_id, array $invoice_state = ['OPEN']) {
    $order = $this->orderManager->load($order_id);
    $invoices = $order->get("field_invoice")->getValue();

    if (count($invoices) == 0) {
      return;
    }

    // Get all invoices.
    foreach ($invoices as $invoice) {
      $state = $invoice["invoice_state"];
      // We check for state change only if state is missing or is OPEN.
      if ($state == NULL || $state == "OPEN") {
        $transaction_id = $invoice["transaction_id"];
        $transaction_timestamp = $invoice["transaction_timestamp"];
        $walle_invoices = $this->walleeWrapper->searchInvoiceByTransaction($transaction_id);
        foreach ($walle_invoices as $wallee_invoice) {
          $new_state = $wallee_invoice->getState();
          $this->setOrderInvoiceState($new_state, $transaction_timestamp, $order);
        }
      }
    }
  }

  /**
   * @param string $state
   * @param int $transaction_timestamp
   * @param Order $order
   */
  public function setOrderInvoiceState(string $state, int $transaction_timestamp, Order $order) {
    for ($i = 0; $i < count($order->get("field_invoice")->getValue()); $i++) {
      $timestamp = $order->get("field_invoice")[$i]->get("transaction_timestamp")
        ->getValue();

      if ($timestamp == $transaction_timestamp) {
        $order->get("field_invoice")[$i]->setState($state);
        $order->save();
      }
    }
  }

  /**
   * @param $order_id
   * @return array|null
   */
  public function getInvoiceEntityByOrderId($order_id) {
    if (gettype($order_id) != "integer" && gettype($order_id) != "string") {
      Drupal::logger('wallee')
        ->notice("transaction id is not a number, its a " . gettype($order_id) . " with value " . $order_id);
      return NULL;
    }
    $query = "select id from invoice_field_data where order_id=" . $order_id;
    $connection = Drupal::database();
    $query = $connection->query($query);
    $result = $query->fetchAll();
    $ids = [];
    foreach ($result as $row) {
      $ids[] = $row->id;
    }

    return $ids;
  }

  /**
   * @param int $invoice_id
   * @param string $invoice_state
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function invoiceStateChanged(int $invoice_id, string $invoice_state) {
    $invoice_ids = $this->getInvoiceEntityIdsByWalleeInvoiceId($invoice_id);
    Drupal::logger('wallee')
      ->notice("drupal invoice entitys which already have the wallee invoice id set");

    // We dont have a wallee invoice yet, so lets create it.
    if (count($invoice_ids) == 0) {
      Drupal::logger('wallee')
        ->notice("none found, so lets get the file from wallee and update the invoice entity");
      $this->getInvoiceFileFromWalleeAndSaveItLocaly($invoice_id, $invoice_state);
      // We do have this invoice, so there must be just a state change.
    }
    else {
      // We need to check if we actually have an order with this invoice attached to it
      // wallee allows for multiple invoices for the same order, each of which might trigger the state change webhook e.g. when they become overdue e.t.c.
      // at this point we must define if the current webhook is about a real valid invoice. if not, just bail out.
      $order = $this->getOrderByInvoiceId($invoice_ids[0]);
      if ($order == NULL) {
        Drupal::logger('wallee')
          ->notice("no order found for this invoice id:" . $invoice_ids[0] . " so, we are bailing out");
        return;
      }

      Drupal::logger('wallee')
        ->notice("found invoice:" . $invoice_ids[0] . " and we are going to change its state");
      $inv_entity = Invoice::load($invoice_ids[0]);
      $inv_entity->setState($invoice_state);
      $inv_entity->save();
    }

    $this->automaticOrderStateChangeAccordingToInvoiceState($invoice_id, $invoice_state);
  }

  /**
   * @param $invoice_id
   * @return array|null
   */
  public function getInvoiceEntityIdsByWalleeInvoiceId($invoice_id) {
    if (gettype($invoice_id) != "integer" && gettype($invoice_id) != "string") {
      Drupal::logger('wallee')
        ->notice("transaction id is not a number, its a " . gettype($invoice_id) . " with value " . implode(",", $invoice_id));
      return NULL;
    }
    $query = "select id from invoice_field_data where invoice_id=" . $invoice_id;
    $connection = Drupal::database();
    $query = $connection->query($query);
    $result = $query->fetchAll();
    $ids = [];
    foreach ($result as $row) {
      $ids[] = $row->id;
    }

    return $ids;
  }

  /**
   * @param $invoice_id
   * @param $invoice_state
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getInvoiceFileFromWalleeAndSaveItLocaly($invoice_id, $invoice_state) {
    Drupal::logger('wallee')
      ->notice("we will try to fetch invoice pdf from drupal, with id:" . $invoice_id);

    // We need the transaction ID.
    $transaction_id = $this->walleeWrapper->searchTransactionByInvoice($invoice_id);
    Drupal::logger('wallee')->notice("transaction id :" . $transaction_id);
    $invoice_entity_id = $this->getInvoiceEntityByTransactionId($transaction_id);
    Drupal::logger('wallee')->notice("invoice entity id:" . $invoice_entity_id);
    // Lets find the Drupal order which has created this TransactionID
    // $order_id = $this->getOrderByTransactionId($transaction_id);
    // $order = $this->orderManager->load($order_id);
    // By now we should already have an Invoice in Wallee
    // Let bring the invoice file locally, for better/faster access.
    $invoiceFile = $this->walleeWrapper->getInvoicesByTransactionId($transaction_id);
    $filename = $invoice_entity_id . ".pdf";
    $private_dir = "private://invoices/";
    file_prepare_directory($private_dir, FILE_CREATE_DIRECTORY);
    $file = file_save_data(base64_decode($invoiceFile), "private://invoices/" . $filename, FILE_EXISTS_RENAME);
    $local_fid = $file->id();
    Drupal::logger('wallee')
      ->notice("downloaded file with local fild:" . $local_fid);
    $timestamp = date('Y-m-d H:i:s', time());

    $inv_info["entity_id"] = $invoice_entity_id;
    $inv_info["invoice_id"] = $invoice_id;
    $inv_info["transaction_id"] = $transaction_id;
    $inv_info["state"] = $invoice_state;
    $inv_info["fid"] = $local_fid;

    // Update the empty invoice with the new info.
    $this->updateInvoice($inv_info);

  }

  /**
   * @param $transaction_id
   * @return int|null
   */
  public function getInvoiceEntityByTransactionId($transaction_id) {

    if (gettype($transaction_id) != "integer" && gettype($transaction_id) != "string") {
      Drupal::logger('wallee')
        ->notice("transaction id is not a number, its a " . gettype($transaction_id) . " with value " . implode(",", $transaction_id));
      return NULL;
    }

    $query = "select id from invoice_field_data where transaction_id=" . $transaction_id;
    $connection = Drupal::database();
    $query = $connection->query($query);
    $result = $query->fetchAll();
    $ret = 0;
    foreach ($result as $row) {
      $ret = $row->id;
      break;
    }
    return $ret;
  }

  /**
   * @param $inv_info
   * @return \Drupal\Core\Entity\EntityInterface|null
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateInvoice($inv_info) {
    Drupal::logger('wallee')
      ->notice("lets update invoice entity:" . $inv_info["entity_id"]);
    $entity_id = $inv_info["entity_id"];
    $invoice_id = $inv_info["invoice_id"];
    $invoice_state = $inv_info["state"];
    $local_fid = $inv_info["fid"];

    // Create the invoice entity.
    $invoiceObj = Invoice::load($entity_id);
    if ($invoiceObj == NULL) {
      Drupal::logger('wallee')
        ->notice("could not load invoice with id:" . $entity_id);
      return NULL;
    }

    $invoiceObj->setInvoiceId($invoice_id);
    // $invoiceObj->setTransactionId($transaction_id);
    $invoiceObj->setState($invoice_state);

    $invoiceObj->invoice_file[] = [
      'target_id' => $local_fid,
    ];

    // Save the invoice.
    $invoiceObj->save();
    return $invoiceObj;
  }

  /**
   * @param $invoice_id
   * @return \Drupal\Core\Entity\EntityInterface|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getOrderByInvoiceId($invoice_id) {
    $invoice = $this->entityTypeManager->getStorage("invoice")
      ->load($invoice_id);
    if ($invoice == NULL) {
      return NULL;
    }

    $order_id = $invoice->getOrderId();
    if ($order_id == NULL || !is_numeric($order_id)) {
      return NULL;
    }

    $order = $this->orderManager->load($order_id);

    return $order;
  }

  /**
   * @param $wallee_invoice_id
   * @param $invoice_state
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function automaticOrderStateChangeAccordingToInvoiceState(int $wallee_invoice_id, string $invoice_state) {
    if ($this->config->get('wallee_invoice_state') != "") {
      $config_state_change = explode(",", $this->config->get('wallee_invoice_state'));
      if (count($config_state_change) > 0 && $config_state_change != "") {
        $inv_state = $config_state_change[0];
        $order_new_state = $config_state_change[1];

        if ($inv_state == $invoice_state) {
          $invoice_entity_ids = $this->getInvoiceEntityIdsByWalleeInvoiceId($wallee_invoice_id);
          if (count($invoice_entity_ids) == 0) {
            return;
          }

          $invoice_entity = $this->invoiceManager->load($invoice_entity_ids[0]);
          $order_id = $invoice_entity->getOrderId();
          $order_entity = $this->orderManager->load($order_id);

          $order_state = $order_entity->getState();
          $order_state_transitions = $order_state->getTransitions();
          $order_state->applyTransition($order_state_transitions[$order_new_state]);

          $order_entity->save();
        }
      }
    }
  }

  /**
   * @param $transaction_id
   * @return string
   */
  public function buildPaymentPageUrl($transaction_id) {
    // $transaction_id = $this->walleeWrapper->searchTransactionByOrderId($order->id());
    return $this->walleeWrapper->buildPaymentPageUrl($transaction_id);
  }

  /**
   * @param $payment_id
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function handleFailedPayment(int $payment_id) {
    $order_state_transitions = $this->getOrderTransitionsByPaymentId($payment_id);
    if ($order_state_transitions == NULL) {
      return;
    }

    $order_entity = $this->getOrderByPaymentId($payment_id);
    if ($order_entity == NULL) {
      return;
    }

    // We should only proceed if payment is the wallee creditcard.
    $paymentPluginId = $this->getOrderPaymentGatewayPluginId($order_entity);
    if ($paymentPluginId !== "wallee_offsite_redirect") {
      return;
    }

    $order_state = $order_entity->getState();

    // If pending, stay there.
    if ($order_state->getValue()["value"] == "pending") {
      return;
    }

    // Else if it can go to pending, go.
    if (in_array('place', array_keys($order_state_transitions))) {
      $order_entity->unlock();
      $order_entity->save();
      $this->loggerFactory->error('wallee payment: made order from draft to pending');
    }
    else {
      $this->loggerFactory->error('wallee payment: COULD NOT make order from draft to pending :' . implode(",", array_keys($order_state_transitions)));
    }

    // else, no idea why we reached this else, so lets log.
    $this->loggerFactory->error('wallee payment: @something', [
      '@something' => "Order payment for order: " . $order_entity->id() . " failed but order already has state :" . $order_state->getValue()["value"],
    ]);

    $this->messenger->addMessage(t('Payment for order ') . $order_entity->id() . t(' has failed'));
  }

  /**
   * @param $payment_id
   * @return |null
   */
  public function getOrderTransitionsByPaymentId(int $payment_id) {
    $order_entity = $this->getOrderByPaymentId($payment_id);
    if ($order_entity == NULL) {
      return NULL;
    }
    $order_state = $order_entity->getState();
    $order_state_transitions = $order_state->getTransitions();
    return $order_state_transitions;
  }

  /**
   * @param $payment_id
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   */
  public function getOrderByPaymentId(int $payment_id) {
    $payment = $this->walleeWrapper->getPaymentInfo($payment_id);
    if (gettype($payment[0]) != "object") {
      Drupal::logger('wallee')
        ->notice("payment[0] is not an object: " . gettype($payment[0]));
      return NULL;
    }
    Drupal::logger('wallee')
      ->notice("payment[0] type is  " . gettype($payment[0]));

    $order_id = $payment[0]->getCharge()
      ->getTransaction()
      ->getMerchantReference();
    $order_entity = $this->orderManager->load($order_id);
    return $order_entity;
  }

  /**
   * @param integer $payment_id
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function handleSuccessfulPayment(int $payment_id) {
    $order_entity = $this->getOrderByPaymentId($payment_id);
    if ($order_entity == NULL) {
      return;
    }
    // We should only proceed if payment is the wallee creditcard.
    $paymentPluginId = $this->getOrderPaymentGatewayPluginId($order_entity);
    if ($paymentPluginId !== "wallee_offsite_redirect") {
      return;
    }

    $order_state = $order_entity->getState();

    $this->loggerFactory->error('wallee payment: @something', ['@something' => "Webhook callback for order: " . $order_entity->id() . " with state SUCCESSFUL is being processed"]);

    $order_state_transitions = $this->getOrderTransitionsByPaymentId($payment_id);
    if ($order_state_transitions == NULL) {
      return;
    }

    $this->loggerFactory->error('wallee payment: @something', ['@something' => "Available transitions for order: " . $order_entity->id() . " : " . print_r($order_state_transitions)]);

    if (in_array('place', array_keys($order_state_transitions))) {
      $this->loggerFactory->error('wallee payment: @something', ['@something' => "Webhook callback for order: " . $order_entity->id() . " order stage will change to PLACED?"]);
      // Otherwise first make pending.
      $order_state->applyTransition($order_state_transitions["place"]);
      $order_entity->save();
    }

    // Get new transitions.
    $order_state_transitions = $this->getOrderTransitionsByPaymentId($payment_id);
    if ($order_state_transitions == NULL) {
      return;
    }

    $this->loggerFactory->error('wallee payment: @something', ['@something' => "Available transitions for order: " . $order_entity->id() . " : " . print_r($order_state_transitions)]);

    if (in_array('paid', array_keys($order_state_transitions))) {
      $this->loggerFactory->error('wallee payment: @something', ['@something' => "Webhook callback for order: " . $order_entity->id() . " order stage will change to PAID"]);
      $order_state->applyTransition($order_state_transitions["paid"]);
      $order_entity->save();
    }

    $this->messenger->addMessage(t('Payment for order ') . $order_entity->id() . t(' was successful'));
    return;
  }

  /**
   * @param OrderInterface $order
   */
  public function derecognizeInvoice(OrderInterface $order) {
    $wallee_invoices = $this->walleeWrapper->searchInvoiceByOrder($order->id());

    if (count($wallee_invoices) == 0) {
      return;
    }

    foreach ($wallee_invoices as $wallee_invoice) {
      $this->walleeWrapper->derecognizeInvoice($wallee_invoice);
    }
  }

  /**
   * For a given order, verify that the invoice file exists.
   *
   * @param OrderInterface $order
   *
   * @return bool
   *
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public function verifyInvoiceExists(OrderInterface $order) {

    $fid = $this->getInvoiceFileByOrderId($order->id());
    if (!$fid) {
      return FALSE;
    }

    /** @var File $file */
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    if (!$file) {
      return FALSE;
    }

    $uri = $file->getFileUri();
    if (file_exists($uri)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Returns the Invoice File fid.
   *
   * @param $order_id
   *
   * @return array
   */
  public function getInvoiceFileByOrderId(int $order_id) {

    $invoice = $this->getInvoiceByOrderId($order_id);
    if (empty($invoice)) {
      return NULL;
    }

    // If multiple invoices, select the latest.
    $keys = array_keys($invoice);
    $max = max($keys);
    return $invoice[$max]->get('invoice_file')->target_id;
  }

  public function getWalleeInvoiceByWalleeInvoiceId(int $wallee_invoice_id) {
    return $this->walleeWrapper->getInvoiceById($wallee_invoice_id);
  }

}
