<?php

namespace Drupal\wallee\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\wallee\WalleeService;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base functionality for the WalleeTransaction Queue Workers.
 */
abstract class WalleeTransactionBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {


  /**
   * @var WalleeService
   */
  protected $walleeService;

  /**
   * WalleeTransactionBase constructor.
   *
   * @param WalleeService $wallee_service
   */
  public function __construct(WalleeService $wallee_service) {
    $this->walleeService = $wallee_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('wallee.wallee_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    try {
      $this->walleeService->createTransactionFromOrderId($data['order_id'], WalleeService::PROCESS_IMMEDIATE);
    } catch (Exception $e) {
      throw new Exception('Queued wallee transaction failed. Item will be retried: ' . $e->getMessage());
    }

  }

}
