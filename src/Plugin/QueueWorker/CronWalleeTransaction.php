<?php

namespace Drupal\wallee\Plugin\QueueWorker;

/**
 * Create Wallee transactions on Cron run.
 *
 * @QueueWorker(
 *   id = "cron_wallee_transaction",
 *   title = @Translation("Create a wallee transaction"),
 *   cron = {"time" = 90}
 * )
 */
class CronWalleeTransaction extends WalleeTransactionBase {

}
