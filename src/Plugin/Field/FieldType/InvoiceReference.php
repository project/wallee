<?php

namespace Drupal\wallee\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type of baz.
 *
 * @FieldType(
 *   id = "field_wallee_invoice_ref",
 *   label = @Translation("Wallee Invoice Reference"),
 *   default_widget = "wallee_invoice_ref_widget",
 *   cardinality = 1
 * )
 */
class InvoiceReference extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $schema['columns'] = [
      'invoice_id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value1 = $this->get('invoice_id')->getValue();

    return empty($value1);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['invoice_id'] = DataDefinition::create('integer')
      ->setLabel(t('Invoice Reference'));

    return $properties;
  }

}
