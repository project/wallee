<?php

namespace Drupal\wallee\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'wallee_invoice_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "wallee_invoice_ref_formatter",
 *   label = @Translation("Wallee Invoice Reference"),
 *   field_types = {
 *     "field_wallee_invoice_ref"
 *   }
 * )
 */

/**
 * Implements ContainerFactoryPluginInterface{.
 */
class InvoiceReferenceFormatter extends FormatterBase {

  protected $entityManager;

  /*
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityManagerInterface $entity_manager) {
  parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

  $this->entityManager = $entity_manager;
  }


  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
  return new static(
  $plugin_id,
  $plugin_definition,
  $configuration['field_definition'],
  $configuration['settings'],
  $configuration['label'],
  $configuration['view_mode'],
  $configuration['third_party_settings'],
  $container->get('entity.manager')
  );
  }
   */

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays links for the attached wallee invoices.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    $count = 0;
    foreach ($items as $delta => $item) {
      $count++;
      $element[$delta] = ['#markup' => "<a target=_blank href='/admin/wallee/invoice/" . $item->value . "'>Invoice " . $count . "</a>"];
    }

    return $element;
  }

}
