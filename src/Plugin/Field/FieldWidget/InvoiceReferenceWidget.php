<?php

namespace Drupal\wallee\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_example_text' widget.
 *
 * @FieldWidget(
 *   id = "wallee_invoice_ref_widget",
 *   module = "wallee",
 *   label = @Translation("Wallee Invoice Reference Widget"),
 *   field_types = {
 *     "field_wallee_invoice_ref"
 *   },
 *   multiple_values = TRUE,
 * )
 */
class InvoiceReferenceWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    if (count($items) > 0) {
      foreach ($items as $item) {

        $invoice_id = $item->get("invoice_id")->getValue();

        $element[] =
          [
            "transaction_id" =>
              [
                '#type' => 'textfield',
                "#title" => "Invoice ID",
                '#default_value' => $invoice_id,
              ],
          ];

      }
    }

    return $element;
  }

}
