<?php

namespace Drupal\wallee_payment\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\wallee\WalleeService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 *
 */
class PaymentOffsiteForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  protected $walleeSrv;

  /**
   * Class constructor.
   */
  public function __construct(WalleeService $walleeSrv) {
    $this->walleeSrv = $walleeSrv;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('wallee.wallee_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var PaymentInterface $payment */
    $payment = $this->entity;
    /** @var OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $redirect_method = $payment_gateway_plugin->getConfiguration()['redirect_method'];
    $remove_js = ($redirect_method == 'post_manual');

    // Gateways that use the GET redirect method usually perform an API call
    // that prepares the remote payment and provides the actual url to
    // redirect to. Any params received from that API call that need to be
    // persisted until later payment creation can be saved in $order->data.
    // Example: $order->setData('my_gateway', ['test' => '123']), followed
    // by an $order->save().
    $order = $payment->getOrder();
    $order_id = $order->id();

    $order->unlock();
    $order->save();

    // We need to create a wallee transaction, in order to get the payment url.
    $transaction_id = $this->walleeSrv->createTransactionFromOrderId($order_id);
    if (!is_numeric($transaction_id)) {
      return;
    }
    // Throw new PaymentGatewayException('Transaction id does not exist');.
    $redirect_url = $this->walleeSrv->buildPaymentPageUrl($transaction_id);

    // Simulate an API call failing and throwing an exception, for test purposes.
    // See PaymentCheckoutTest::testFailedCheckoutWithOffsiteRedirectGet().
    if ($order->getBillingProfile()->get('address')->family_name == 'FAIL') {
      throw new PaymentGatewayException('Could not get the redirect URL.');
    }

    $data = [
      'return' => $form['#return_url'],
      'cancel' => $form['#cancel_url'],
      'total' => $payment->getAmount()->getNumber(),
    ];

    $form = $this->buildRedirectForm($form, $form_state, $redirect_url, $data, $redirect_method);
    if ($remove_js) {
      // Disable the javascript that auto-clicks the Submit button.
      unset($form['#attached']['library']);
    }

    return $form;
  }

}
