<?php

namespace Drupal\wallee_payment\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Session\AccountProxy;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * This is a dummy controller for mocking an off-site gateway.
 */
class AfterPaymentController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The current request.
   *
   * @var Request
   */
  protected $currentRequest;

  protected $currentUser;

  protected $cartProvider;

  protected $messenger;

  /**
   * Constructs a new DummyRedirectController object.
   *
   * @param RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack, AccountProxy $currentUser, CartProviderInterface $cart_provider, MessengerInterface $messenger) {
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->currentUser = $currentUser;
    $this->cartProvider = $cart_provider;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('current_user'),
      $container->get('commerce_cart.cart_provider'),
      $container->get('messenger')
    );
  }

  /**
   *
   */
  public function paymentFailed() {
    $carts = $this->cartProvider->getCarts($this->currentUser);
    $cart_id = array_pop(array_reverse($carts))
      ->get("order_id")
      ->getValue()[0]["value"];
    $url = Url::fromRoute('commerce_checkout.form',
      ['commerce_order' => $cart_id, 'step' => 'order_information']
    );

    $path = $this->currentRequest->getSchemeAndHttpHost() . "/" . $url->toString();

    $this->messenger->addWarning($this->t("Your payment attempt failed. Please try again."));
    $response = new RedirectResponse($path);
    $response->send();
    return;
  }

}
